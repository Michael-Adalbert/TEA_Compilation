package Automate.Erreur.Parsing;

public class LineTypeUndifined extends Exception {
    private Integer indexLigne;
    private Character type;
    private String ligne;

    /**
     * @param i le numero de la ligne de l'erreur
     * @param c le char non lu
     * @param s la ligne en entier
     */
    public LineTypeUndifined(Integer i, Character c, String s) {
        super();
        this.indexLigne = i;
        this.type = c;
        this.ligne = s;

    }

    @Override
    public String toString() {
        return "Erreur à la ligne : " + this.indexLigne + "\n" +
                "Le Character " + this.type + " n'est pas reconnu durant le traitement du fichier \n" +
                "Ligne : " + this.ligne + "\n";
    }
}
