package Automate.Outils;

public class Pair<A, B> {
    private final A a;
    private final B b;

    public Pair(A a, B b) {
        this.a = a;
        this.b = b;
    }

    public B getB() {
        return b;
    }

    public A getA() {
        return a;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Pair) {
            Pair p = (Pair) obj;
            return this.a.equals(p.a) && this.b.equals(p.b);
        }
        return false;
    }

    @Override
    public String toString() {
        return "(" + this.a + "," + this.b + ")";
    }
}
