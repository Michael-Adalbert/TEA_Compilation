package Automate;

import Automate.Erreur.Parsing.LineTypeUndifined;
import Automate.Outils.Pair;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static java.nio.file.Files.createFile;

public class Automate {

    private final static Scanner SC = new Scanner(System.in);

    private final String com;
    private final Character metaChar;
    private final ArrayList<Character> alphaIn;
    private final ArrayList<Character> alphaOut;
    private final ArrayList<Integer> listEtat;
    private final ArrayList<Integer> etatEntrer;
    private final ArrayList<Integer> etatSortie;
    private final ArrayList<Pair<Pair<Integer, Character>, Pair<Integer, Character>>> transitions;

    private Automate(String commentaire,
                     Character metaChar,
                     ArrayList<Character> alphaIn,
                     ArrayList<Character> alphaOut,
                     ArrayList<Integer> listEtat,
                     ArrayList<Integer> etatEntrer,
                     ArrayList<Integer> etatSortie,
                     ArrayList<Pair<Pair<Integer, Character>, Pair<Integer, Character>>> transitions) {
        this.com = commentaire;
        this.metaChar = metaChar;
        this.alphaIn = alphaIn;
        this.alphaOut = alphaOut;
        this.listEtat = listEtat;
        this.etatEntrer = etatEntrer;
        this.etatSortie = etatSortie;
        this.transitions = transitions;
    }

    /**
     * Lit un fichier .descr dont le chemin est passe en parametre
     *
     * @param path le chemin du fichier à lire
     * @return un nouvel Automate crée à partir des specification du fichier
     */
    public static Automate readDescrFile(String path) {


        //Contient le comentaire du Fichier ligne C
        StringBuilder com = new StringBuilder();
        //meta Character par defaut '#' il represente le lambda
        Character metaChar = '#';

        //Données de l'automate
        ArrayList<Character> alphaIn = new ArrayList<>();
        ArrayList<Character> alphaOut = new ArrayList<>();
        ArrayList<Integer> listEtat = new ArrayList<>();
        ArrayList<Integer> etatEntrer = new ArrayList<>();
        ArrayList<Integer> etatSortie = new ArrayList<>();
        ArrayList<Pair<Pair<Integer, Character>, Pair<Integer, Character>>> transitions = new ArrayList<>();

        //on definit l'etat d'entrer par defaut (0)
        etatEntrer.add(0);


        try {
            //lecteur du fichier il le lit ligne par ligne
            BufferedReader reader = new BufferedReader(new FileReader(path));
            //Prend une la premiere ligne
            String line = reader.readLine();
            //Index pour savoir ou se trouve l'erreur
            Integer indexLigne = 0;
            while (line != null) {
                //Split les information de la ligne avec comme separateur l'espace " "
                String[] dataLine = line.split(" ");

                //Prend le premier Charactere qui dit quel type de ligne on a
                Character lineType = dataLine[0].charAt(0);
                switch (lineType) {
                    case 'C':
                        // Traite la ligne C
                        for (int i = 1; i < dataLine.length; i++) {
                            com.append(dataLine[i]).append(" ");
                        }
                        break;
                    case 'M':
                        // Traite la Ligne de type M
                        // On pretent que on a que 1 seul charactere
                        metaChar = dataLine[1].charAt(0);
                        break;
                    case 'V':
                        // Traite la Ligne de type V
                        String strAlphaSortieEntree = dataLine[1];
                        //On split chaque character pour les ajouter à l'alphabet
                        for (String letter : strAlphaSortieEntree.replaceAll("\"", "").split("")) {
                            alphaIn.add(letter.charAt(0));
                        }
                        break;
                    case 'O':
                        //Traite la Ligne de type O
                        String strAlphaSortie = dataLine[1];
                        //meme traitement que pour les Ligne V
                        for (String letter : strAlphaSortie.replaceAll("\"", "").split("")) {
                            if (letter.length() == 1) {
                                alphaOut.add(letter.charAt(0));
                            }
                        }
                        break;
                    case 'E':
                        //Traite la ligne de typee E
                        String strNbEtat = dataLine[1];
                        int nbEtat = Integer.parseInt(strNbEtat);
                        //En Fonction du nombre d'etat ajoute tout les etat de 0 a .. nbEtat
                        for (int i = 0; i < nbEtat; i++) {
                            listEtat.add(i);
                        }
                        break;
                    case 'I':
                        //Traite la Ligne de Type I
                        etatEntrer.clear();
                        //Lit tout les etat initiaux possible et les ajoute
                        for (Integer i = 1; i < dataLine.length; i++) {
                            etatEntrer.add(Integer.parseInt(dataLine[i]));
                        }
                        break;
                    case 'F':
                        //Traite les ligne de type F
                        for (Integer i = 1; i < dataLine.length; i++) {
                            etatSortie.add(Integer.parseInt(dataLine[i]));
                        }
                        break;
                    case 'T':
                        //Traite les Ligne de Type T
                        //dep etat de depart
                        Integer dep = Integer.parseInt(dataLine[1]);

                        //On lit l'etat lu
                        Character lu = dataLine[2].replaceAll("'", "").charAt(0);

                        //On lit l'etat d'arriver
                        Integer arr = Integer.parseInt(dataLine[3]);

                        //Par defaut on ecrit rien MetaChar
                        Character ecrit = metaChar;

                        //Si il y a 5 case c'est que l'on a un charactere defini
                        if (dataLine.length == 5) {
                            ecrit = dataLine[4].replaceAll("'", "").charAt(0);
                        }
                        //On ajoute la transition
                        /*
                            Modele
                            Couple de Couple
                            ( Depart , Arriver )
                            Depart = Couple de ( Etat depart entier , charactere lu character )
                            Arriver = Couple de (Etat arriver entier , charactere ecrit character )
                         */

                        transitions.add(new Pair<>(new Pair<>(dep, lu), new Pair<>(arr, ecrit)));
                        break;
                    default:
                        // si le character n'est pas reconnu , lance une erreur
                        throw new LineTypeUndifined(indexLigne, dataLine[0].charAt(0), line);
                }
                line = reader.readLine();
                indexLigne++;
            }
            // Renvoi le nouvel Automate
            return new Automate(com.toString(), metaChar, alphaIn, alphaOut, listEtat, etatEntrer, etatSortie, transitions);
        } catch (IOException e) {
            return null;
        } catch (LineTypeUndifined lineTypeUndifined) {
            System.err.println(lineTypeUndifined);
        }
        return null;
    }

    /**
     * retourne les etats suivant posibble un seul si l'automate est deterministe
     *
     * @param etat   etat actuel
     * @param lettre charactere lu par l'automate
     * @param out    stringbuilder dans lequel on ecrit la sorti de l'automate
     * @return un ensemble d'etat suivant avec les sorties correspondantes
     */
    private ArrayList<Pair<Integer, StringBuilder>> getNextState(Integer etat, Character lettre, StringBuilder out) {

        //Init

        ArrayList<Pair<Integer, Character>> next = new ArrayList<>();
        ArrayList<Pair<Integer, StringBuilder>> nextStates = new ArrayList<>();
        Pair<Integer, Character> current = new Pair<>(etat, lettre);

        //Traitement

        //Est ce que la lettre appartient à l'alphabet ?
        if (this.alphaIn.contains(lettre)) {

            //Pour chaque transition
            for (Pair<Pair<Integer, Character>, Pair<Integer, Character>> tr : this.transitions) {

                //Si l'etat initial est respecter et le character aussi
                //Sinon si c'est un metacharacter alors on execute comme si on etat a l'etat suivant
                if (tr.getA().getA().equals(current.getA()) && tr.getA().getB().equals(current.getB())) {

                    //on ajoute le couple "out"
                    next.add(tr.getB());

                    //Si il y a une sortie alors afficher cette sortie
                    if (!this.metaChar.equals(tr.getB().getB())) {
                        System.out.print(", Sortie : " + tr.getB().getB());
                    }

                } else if (tr.getA().getA().equals(current.getA()) && tr.getA().getB().equals(this.metaChar)) {
                    nextStates.addAll(this.getNextState(tr.getB().getA(), lettre, new StringBuilder(out)));
                }
            }

            //Affichaqe si il y a des transition possible
            if (!next.isEmpty()) {
                System.out.print(" Transition trouvée ");
            }

            //Pour chaque prochaine etat
            for (Pair<Integer, Character> p : next) {

                //Prochaine etat
                Integer nextI = p.getA();

                //Copie initial de la sortie
                StringBuilder newNextOut = new StringBuilder(out.toString());

                //Nouvelle etat courant
                Pair<Integer, StringBuilder> newNext = new Pair<>(nextI, newNextOut);

                //Si la sortie n'est pas le meta character alors on l'append à la sortie
                if (!p.getB().equals(this.metaChar)) {
                    newNextOut.append(p.getB());
                }

                //On l'ajoute au resultat
                nextStates.add(newNext);
            }
        }
        return nextStates;
    }

    /**
     * @param in Mot a test
     * @return les mot qu'il a créé
     */
    private ArrayList<String> accept(String in) {

        //Init start state
        ArrayList<Pair<Integer, StringBuilder>> currentState = new ArrayList<>();

        //Si non deterministe currentState Size > 1 sinon Size = 1
        for (Integer i : this.etatEntrer) {
            currentState.add(new Pair<>(i, new StringBuilder()));
        }

        ArrayList<String> result = new ArrayList<>();

        //Etat Suivant
        ArrayList<Pair<Integer, StringBuilder>> nextState = new ArrayList<>();

        boolean estAcceptant = false;

        // On commence index 0 du mot
        Integer indexWord = 0;
        Character currentChar;


        //Si la taille du mot > 0
        if (in.length() > 0) {
            //Tant que l'on a des character à lire
            while (indexWord < in.length()) {
                //On lit le character
                currentChar = in.charAt(indexWord);

                //Pour chaque etatCourant
                for (Pair<Integer, StringBuilder> etatCourant : currentState) {
                    Integer etatIn = etatCourant.getA();
                    System.out.print("Etat Courant : " + etatIn + ", Entrée : " + currentChar);

                    //On recupere les prochains etats courant possible
                    ArrayList<Pair<Integer, StringBuilder>> res = this.getNextState(etatIn, currentChar, etatCourant.getB());
                    System.out.print("Etat Suivant :");
                    for (Pair<Integer, StringBuilder> next : res) {
                        System.out.print(" " + next.getA());
                    }
                    System.out.println();
                    nextState.addAll(res);
                }
                //On vide la list des etatCourant et on met les nouveaux
                currentState.clear();
                currentState.addAll(nextState);
                nextState.clear();

                //On passe au prochain character
                indexWord++;
            }

            System.out.println("------------- Fin de chaine --------------");


            result = new ArrayList<>();


            //Pour chaque etat de fin
            for (Pair<Integer, StringBuilder> finalStatePair : currentState) {
                //Si l'etat dans lequel il est est dans etatSortie alors il est acceptant
                if (this.etatSortie.contains(finalStatePair.getA())) {
                    estAcceptant = true;
                }
                //si le resultat n'existe pas on ajoute le resultat
                String res = finalStatePair.getB().toString();
                if (!result.contains(res)) {
                    result.add(finalStatePair.getB().toString());
                }
            }
        } else {
            ArrayList<Integer> inOut = this.lambdaFermeture(this.etatEntrer);


            for (Integer i : inOut) {
                if (this.etatSortie.contains(i)) {
                    result.add("");
                    estAcceptant = true;
                }
            }
        }
        if (estAcceptant) {
            System.out.println("Entrée acceptante");
        } else {
            System.out.println("Entrée non-acceptante");
        }
        return result;
    }

    /**
     * Procedure de Test des Mot sur l'automate
     */
    public void testMot() {
        //Init

        ArrayList<String> listMot = new ArrayList<>(), resultats;
        String entrerMot;

        // Traitement

        System.out.println("-----------Traitement Phrase--------------");

        System.out.println("Veuillez saisir les phrases à lire :");
        System.out.println("(Chaque phrase est terminée par ENTREE.");
        System.out.println("La lecture des phrases est terminée par ###)");

        // Lit les phrases tant que la chaine n'est pas ### 
        entrerMot = SC.nextLine();
        while (!"###".equals(entrerMot)) {
            listMot.add(entrerMot);
            entrerMot = SC.nextLine();
        }

        System.out.println("Traitement des phrases :");

        for (String mot : listMot) {

            System.out.println("Nous traitons le mot : " + mot);

            resultats = this.accept(mot);

            for (String res : resultats) {
                System.out.println("La sortie de cette phrase est : " + res);
            }

            System.out.println("------------- Fin de phrase --------------");
        }

        System.out.println("------------------------------------------");
    }

    /**
     * @param etat ensemble d'etat sur lequel executer la lambda fermeture
     * @return un super etat
     */
    private ArrayList<Integer> lambdaFermeture(List<Integer> etat) {
        ArrayList<Integer> F = new ArrayList<>();
        ArrayList<Integer> p = new ArrayList<>(etat);

        while (!p.isEmpty()) {
            Integer t = p.remove(p.size() - 1);
            if (!F.contains(t)) {
                F.add(t);
                for (Pair<Pair<Integer, Character>, Pair<Integer, Character>> tr : this.transitions) {
                    if (this.metaChar.equals(tr.getA().getB()) && t.equals(tr.getA().getA())) {
                        p.add(tr.getB().getA());
                    }
                }
            }
        }

        F.sort(Integer::compareTo);

        return F;
    }

    /**
     * @param etat etat sur lequel effectuer la transition
     * @param a    la lettre a prendre en compte
     * @return list
     */
    private ArrayList<Integer> transiter(List<Integer> etat, Character a) {
        ArrayList<Integer> f = new ArrayList<>();
        for (Integer i : etat) {
            for (Pair<Pair<Integer, Character>, Pair<Integer, Character>> tr : this.transitions) {
                if (i.equals(tr.getA().getA()) && a.equals(tr.getA().getB())) {
                    Integer u = tr.getB().getA();
                    if (!f.contains(u)) {
                        f.add(u);
                    }
                }
            }
        }

        return f;
    }

    /**
     * effectue la determinisation sur l'automate (this)
     *
     * @return un nouvel automate deterministe
     */
    public Automate determiner() {
        Automate a = new Automate(this.com, this.metaChar, this.alphaIn, this.alphaOut, new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), new ArrayList<>());

        ArrayList<ArrayList<Integer>> p = new ArrayList<>();
        ArrayList<Pair<Pair<Integer, Character>, Pair<Integer, Character>>> d = new ArrayList<>();

        p.add(this.lambdaFermeture(this.etatEntrer));

        for (Integer i = 0; i < p.size(); i++) {
            ArrayList<Integer> etat = p.get(i);
            int indexNoeud = p.indexOf(etat);
            for (Character lettre : this.alphaIn) {
                ArrayList<Integer> nextEtat = this.lambdaFermeture(this.transiter(etat, lettre));
                Integer nextIndex;
                if (!estDedans(p, nextEtat)) {
                    p.add(nextEtat);
                }
                nextIndex = p.indexOf(nextEtat);
                Pair<Pair<Integer, Character>, Pair<Integer, Character>> newTransition = new Pair<>(new Pair<>(indexNoeud, lettre), new Pair<>(nextIndex, a.metaChar));
                d.add(newTransition);
            }
        }

        a.etatEntrer.add(0);
        for (Integer i = 0; i < p.size(); i++) {
            a.listEtat.add(i);

            ArrayList<Integer> etat = p.get(i);

            for (Integer etatFinal : this.etatSortie) {
                if (etat.contains(etatFinal)) {
                    a.etatSortie.add(i);
                }
            }


        }
        a.transitions.addAll(d);


        return a;
    }

    private static boolean estDedans(ArrayList<ArrayList<Integer>> tab, ArrayList<Integer> v) {
        boolean b = false;
        int i = 0;
        while (i < tab.size() && !b) {
            if (tab.get(i).equals(v)) {
                b = true;
            }
            i++;
        }
        return b;
    }

    /**
     * @return une chaine de character au format DESCR
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (!"".equals(com)) {
            sb.append("C ").append(com).append("\n");
        }
        sb.append("M ").append(metaChar).append("\n").append("V ").append("\"");
        for (Character a : this.alphaIn) {
            sb.append(a);
        }
        sb.append("\"").append("\n").append("O ").append("\"");
        for (Character a : this.alphaOut) {
            sb.append(a);
        }
        sb.append("\"").append("\n").append("E ").append(this.listEtat.size()).append("\n").append("I ");
        for (Integer a : this.etatEntrer) {
            sb.append(a).append(" ");
        }
        sb.append("\n").append("F ");
        for (Integer a : this.etatSortie) {
            sb.append(a).append(" ");
        }
        sb.append("\n");
        for (Pair<Pair<Integer, Character>, Pair<Integer, Character>> p : this.transitions) {
            Pair<Integer, Character> k = p.getA();
            Pair<Integer, Character> v = p.getB();
            sb.append("T ").append(k.getA()).append(" ").append("'").append(k.getB()).append("'");
            sb.append(" ").append(v.getA()).append(" ").append("'").append(v.getB()).append("'");
            sb.append("\n");
        }
        return sb.toString();
    }

    /**
     * Sauvegarde le L'objet sous forme d'un fichier DESCR
     *
     * @param chemin chemin vers OU on veut le sauvegarder
     * @param name   sous quel nom on veut le sauvegarder
     */
    private void exportDescr(String chemin, String name) {
        if ("".equals(chemin)) {
            chemin = ".";
        }
        Path testPath = Paths.get(chemin + "/" + name + ".descr");
        File testFile = testPath.toFile();
        if (testFile.exists()) {
            if (testFile.delete()) {
                System.out.print("");
            }
        }

        try {
            Path p = createFile(testPath);
            File f = p.toFile();
            Writer w = new BufferedWriter(new FileWriter(f));
            w.write(this.toString());
            w.flush();
            w.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sauvegarde le L'objet sous forme d'un fichier DOT
     *
     * @param chemin chemin vers OU on veut le sauvegarder
     * @param name   sous quel nom on veut le sauvegarder
     */
    private String exportDot(String chemin, String name) {
        if ("".equals(chemin)) {
            chemin = ".";
        }
        Path where = Path.of(chemin + "/" + name + ".dot");
        if (where.toFile().exists()) {
            if (where.toFile().delete()) {
                System.out.print("");
            }
        }
        try {
            Path p = createFile(where);
            File f = new File(String.valueOf(p.toAbsolutePath()));
            Writer w = new BufferedWriter(new FileWriter(f));
            w.write("digraph G {\n");
            for (Pair<Pair<Integer, Character>, Pair<Integer, Character>> tr : this.transitions) {
                w.write("\t" + tr.getA().getA() + " -> " + tr.getB().getA() + "[label=\"" + tr.getA().getB() + "|" + tr.getB().getB() + "\"];\n");
            }
            for (Integer etatFinal : this.etatSortie) {
                w.write("\t" + etatFinal + "[peripheries=2];\n");
            }
            for (Integer etatInitial : this.etatEntrer) {
                w.write("\t" + etatInitial + "[shape=box];\n");
            }
            w.write("}");
            w.flush();
            w.close();

            return p.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public void saveAsDescr(String nomFichier, String ajoutNom) {
        System.out.print("Voulez vous enregistrer au format DESCR [Y|N] ? :");
        String res = SC.nextLine();
        if (res.matches("^[Y|y](es)?")) {

            this.exportDescr("outDescr/", nomFichier + ajoutNom);
        }
        System.out.println("------------------------------------------");

    }

    public void saveAsDot(String nomFichier, String ajoutNom) {
        System.out.print("Voulez vous enregistrer au format DOT [Y|N] ? :");
        String res = SC.nextLine();
        if (res.matches("^[Y|y](es)?")) {
            String path = this.exportDot("outDot/", nomFichier + ajoutNom);
            if (!path.isEmpty()) {
                try {
                    Runtime.getRuntime().exec("dot -Tjpg " + path + " -o outJPG/" + nomFichier + ajoutNom + ".jpg");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
        System.out.println("------------------------------------------");
    }

    /**
     *
     * @return vrai si l'automate est deterministe
     */
    public boolean estDeterministe() {
        Boolean estDeterministe = true;
        Integer i = 0;

        if (this.etatEntrer.size() > 1) {
            estDeterministe = false;
        }

        while (i < this.listEtat.size() && estDeterministe) {
            Integer etat = listEtat.get(i);
            for (Character a : alphaIn) {
                Integer compteur = 0;

                for (Pair<Pair<Integer, Character>, Pair<Integer, Character>> tr : this.transitions) {
                    if (tr.getA().getA().equals(etat) && tr.getA().getB().equals(a)) {
                        compteur += 1;
                    } else if (tr.getA().getB().equals(this.metaChar)) {
                        estDeterministe = false;
                    }
                }
                if (compteur > 1) {
                    estDeterministe = false;
                }
            }
            i++;
        }

        return estDeterministe;
    }
}
