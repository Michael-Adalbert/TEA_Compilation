import Automate.Automate;

import java.util.Scanner;

public class Compilateur {

    private static Scanner sc = new Scanner(System.in);
    private static String nomFichier = "";

    public static void main(String[] args) {

        //Init

        String path, ajout = "";
        String[] tabPath;
        Automate automate;

        //Programme

        //Bloc de recherche de l'automate

        System.out.println("-------------------Start------------------");
        do {
            System.out.print("Entrez le nom de l'automate : ");
            path = sc.nextLine();
            tabPath = path.split("/");
            nomFichier = tabPath[tabPath.length - 1];
            automate = Automate.readDescrFile(path + ".descr");
        } while (automate == null);

        System.out.println("------------------------------------------");

        if (!automate.estDeterministe()) {
            ajout = "_Determiner";
            System.out.println("\n-----L'automate n'est pas deterministe----\n");
            System.out.println("---------Determinisation Start------------");
            System.out.print("Voulez vous le Sauvegarder avant de le determiner [Y|N] ?");
            String res = sc.nextLine();
            if (res.matches("^[Y|y](es)?")) {
                allSave(automate, "");
            }
            automate = automate.determiner();
            System.out.println("\n---------Determinisation Finie------------\n");
        }

        automate.testMot();

        //Bloc de sauvegarde
        allSave(automate, ajout);


        System.out.println("-----------------Finish-------------------");

    }

    private static void allSave(Automate a, String ajout) {
        System.out.println();
        a.saveAsDescr(nomFichier, ajout);
        System.out.println();
        a.saveAsDot(nomFichier, ajout);
    }

}


